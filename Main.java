public class Main {

    public static int ceilingLinearSearch(int[] arr, int n, int x) {
        for (int i = 0; i < n; i++) {
            if (arr[i] >= x)
                return i;
        }
        return -1;
    }

    public static int ceilingBinarySearch(int[] arr, int n, int x) {
        int l = 0, r = n - 1;
        int m = -1;
        while (l <= r) {
            m = l + (r - l) / 2;
            if (arr[m] == x) {
                return m;
            }
            if (arr[m] < x) {
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        if (m == 0)
            return 0;
        if (m == n - 1)
            return -1;
        return m + 1;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 6, 7};
        int pos = ceilingBinarySearch(arr, arr.length, 1);
        System.out.println(pos == -1 ? "Khong ton tai" : arr[pos]);
    }
}
